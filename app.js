var express = require('express'),
    http = require('http'),
    path = require('path'),
    util = require('util'),
    _ = require('underscore'),
    _str = require('underscore.string');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });
  app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

/*
  Ideally, we would refactor the below routes into a routes or controllers folder
  and make registering them easier with an auto requiring helper that we
  can just point at the folder/*.
*/

app.get('/', function (req, res){
  res.send('OK');
});

app.post('/reverse', function (req, res){
  res.send(_str.reverse(req.body.theString));
});

app.get('/:city-:state-:title', function (req, res) {
  var city = req.params.city,
      state = req.params.state,
      title = req.params.title;
  res.send({
    city: city,
    state: state
  });
});

app.post('/sort', function (req, res) {
  var theArray = JSON.parse(req.body.theArray);
  console.log(util.inspect(theArray));
  var noStrings = _.select(theArray, function (item) {
    return _.isString(item);
  });
  console.log(util.inspect(noStrings));
  var sorted = _.sortBy(noStrings, function (item) {
    return item;
  });
  console.log(util.inspect(sorted));
  res.send(JSON.stringify(sorted));
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});